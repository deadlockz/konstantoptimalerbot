package main

import(
    "bytes"
    "net/http"
    "fmt"
    "io/ioutil"
    "os"
    "strings"
    "encoding/json"
)

const BOT_TOKEN = "0815081542:AAAAAAAAAAAAAAAAAAAAAw3__popo"
const BOT_DOMAIN = "bot.yourdomain.com"

type Result struct {
    Type           string  `json:"type"`
    Id             string  `json:"id"`
    Title          string  `json:"title"`
    MessageText    string  `json:"message_text"`
}

type Answer struct {
    Method          string      `json:"method"`
    InlineQueryId   string      `json:"inline_query_id"`
    Results         []Result    `json:"results"`
}

type InlineQuery struct {
    ID      string  `json:"id"`
    Query   string  `json:"query"`
}

type Update struct {
    UpdateID        uint64          `json:"update_id"`
    InlineQuery     *InlineQuery    `json:"inline_query"`
}

func apiRequestJson(method string, parameters Answer) {
    parameters.Method = method
    marshalled, _ := json.Marshal(parameters)
    request, _ := http.NewRequest(
        "POST",
        "https://api.telegram.org/bot" + BOT_TOKEN + "/",
        bytes.NewBuffer(marshalled))
    request.Header.Set("Content-Type", "application/json")
    client := &http.Client{}
    response, _ := client.Do(request)
    defer response.Body.Close()
}

func main() {
    if (len(os.Args) < 2) {
        fmt.Println("missing a port number")
        os.Exit(2)
    }
    PORT := os.Args[1]

    f, _ := os.Create("setwebhook-and-upload-selfsigned.html")
    f.WriteString("<html><body>\n" +
        "Do this <tt>openssl req -newkey rsa:2048 -sha256 -nodes -keyout mypriv.key -x509 -days 3650 " +
        " -out myssl.pem -subj \"/C=DE/ST=NRW/L=Bielefeld/O=The not existing person/CN=" + BOT_DOMAIN + "\"</tt><hr>\n" +
        "<form method=\"post\" enctype=\"multipart/form-data\"" +
        " action=\"https://api.telegram.org/bot" + BOT_TOKEN  + "/setWebhook\">\n" + 
        "<input type=\"hidden\" name=\"url\" value=\"https://" + BOT_DOMAIN + ":" + PORT + "/\" />" +
        "<input type=\"file\" name=\"certificate\" />\n" +
        "<input type=\"submit\" />\n</form></body></html>\n")
    defer f.Close()
    
    http.HandleFunc( "/", func(resp http.ResponseWriter, req *http.Request) {
        content, _ := ioutil.ReadAll(req.Body)
        var update Update
        json.Unmarshal([]byte(content), &update)

        if (update.InlineQuery != nil) {
            update.InlineQuery.Query = strings.Replace(update.InlineQuery.Query, "\"","''",-1)
            queryText := update.InlineQuery.Query 
            queryText += " relly? I think your situation is still constant optimal. Thank you."
            var answer Answer
            answer.InlineQueryId = update.InlineQuery.ID
            aResult := Result {
                "article",
                "0",
                "What I think ...",
                queryText }
            bResult := Result {
                "article",
                "1",
                "hm ...",
                "Ask me later." }
            answer.Results = []Result{aResult,bResult}
            
            apiRequestJson("answerInlineQuery", answer)
        }
    })
    
    err := http.ListenAndServeTLS(
        ":" + PORT,
        "myssl.pem",
        "mypriv.key",
        nil)

    if (err != nil) {
        fmt.Println(err)
        os.Exit(3)
    }
}



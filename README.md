# KonstantOptimalerBot

A minimal tutorial to create and run an Telegram inline Bot with Golang.

## (not) requiments

- You do not need a `let's Encrypt` or payed ssl certificate
- You do not need external Golang code from Telegram
- You need a server which can run Golang stuff
- You need a root login to configure and bind to port 80, 88, 443 or 8443
- You need a domain (or subdomain of a domain: e.g. `bot.yourdomain.com`)
- You need a Telegram Client to talk to `@BotFather`

If you want to make (additionally) photo results in the future, only
port 443 works good. Because there is a bug in the Telegram-Desktop Code this
photo result will not be shown there. Maybe the telegram Bot API discription
have holes here. You should use some Telegram API golang code
to let photo results work correct. On Telegram-Android: photo-results are
possible, but you need a payed or `let's Encrypt` ssl certificate. To make
this tutorial easy (?), we use a self signed cert and upload it to
telegram (with a self made html form).

## how does it work (step 1)

First I have seen the Telegram API (and later the BOT API) description and
I got confused. I saw some tables with data types an functions and I do
not know how to use it. The trick is "json calls" and "json results" or
somethink like that. After "talking" to `@BotFather` to make a new bot,
you got a BOT_TOKEN like `0815081542:AAAAAAAAAAAAAAAAAAAAAw3__popo`. After
that (for this example) say `/setinline` to BotFather and configure a
placeholder text. ATTENTION: this BOT_TOKEN is very private!!!!

Now we have everything to talk to the Telegram Bot system!

I first try is that "page" you can call (but did it in a save way an not in
a online chat cafe shop):

```
https://api.telegram.org/bot0815081542:AAAAAAAAAAAAAAAAAAAAAw3__popo/getWebhookInfo
```

You see/replace the TOKEN after `api.telegram.org/bot` in that request and
a command `getWebhookInfo` at the end. you get a json string as result and
it says something like "false" or "webook not set". That is ok, because
you have not set it at that time.

In HTTP(s) the normal way is to use GET or POST to send additional data
with a request (like the above one). The trick of the Telegram (BOT) API
is: replacing the Content in GET/POST with JSON strings to submit objects
and parameters to the commands like e.g. `setWebhook`. That is all the magic!

## step 2... and more

For the first `go run koBot.go 8443` as root user (root for testing!) or
doing `go build` and then `./KonstantOptimalerBot 8443` you have to
edit two constans in `koBot.go`:

```
const BOT_TOKEN = "0815081542:AAAAAAAAAAAAAAAAAAAAAw3__popo"
const BOT_DOMAIN = "bot.yourdomain.com"
```

Place your TOKEN (you get it from `@BotFather` on Telegram) and your DOMAIN
there! The next step is generating a self signed ssl cert:

```
openssl req -newkey rsa:2048 -sha256 -nodes -keyout myssl.key -x509 -days 3650 \
  -out myssl.pem -subj "/C=DE/ST=NRW/L=Bielefeld/O=Umbrella Corp/CN=bot.yourdomain.com"
```

It creates a `myssl.pem` cert (and a `myssl.key` private key used for that cert)
valid for 10 years (`days 3650`). This example:

- C country: DE
- ST state: NRW
- L location: Bielefeld
- O organisation: Umbrella Corp
- CN common name: bot.yourdomain.com

It is important to place your DOMAIN here! The other parts seems to be set, but
not (?) important for a self signed cert.

You can do (as root) `go run koBot.go 8443` now! If you want to use 80, 88 or 433
as port, replace 8443 here!

## Hm. How does the Telegram knows my server/webhook?

Good question! If you just use a `let's encrypt` cert you do not need to upload
the cert to telegram. Then setting the webhook url to your server is just
that call (use curl or web-browser):

```
https://api.telegram.org/bot0815081542:AAAAAAAAAAAAAAAAAAAAAw3__popo/setWebhook?url=https://bot.yourdomain.com:8443/
```

Still take attention, that your BOT_TOKEN `0815081542:AAAAAAAAAAAAAAAAAAAAAw3__popo` never
gets public! You may revoke it via `@BotFather` if something wents wrong and someone
gets the token and replaces bot. The Wehook url *IS public* and do not change
the bot that way, that the TOKEN gets public by the golang code!

Ok, but this example use a self signed cert and we have to upload it together
with the url. The simplist way to do that is a simple html form. Based on
the PORT, DOMAIN and TOKEN the Golang code creates this html-form to you:

```
setwebhook-and-upload-selfsigned.html
```

The content is very simple and similar to that:

```
<html>
<body>
<form
 method="post"
 enctype="multipart/form-data"
 action="https://api.telegram.org/bot.BOT_TOKEN./setWebhook">
<input type="hidden" name="url" value="https://.BOT_DOMAIN.:.PORT./" />
<input type="file" name="certificate" />
<input type="submit" />
</form>
</body>
</html>
```

The Golang code replaces TOKEN (!!), DOMAIN and PORT for you. Copy the
generated `html` file and the `myssl.pem` to a secure system to open
the html file in a browser. Now click on "Choose File" and select the `myssl.pem`
to upload/submit it to the Telegram system. After a submit you got
some JSON string back like "true", "success" or a failure hints(?).

Thats it! Sometimes it takes some hours (3 or 20) to test your inline Bot
without any delays, stange caching behavior or a missing "via @....Bot"
hint.

AGAIN: in that html form is your BOT_TOKEN. Never publish the token!

## the code - some hints

- Ok, on many places I do not catch expected errors. Feel free to do that different!
- Some JSON golang examples uses `interface{}`. DO NOT DO THAT! I always struggling
  with that! For a clean `JSON.Marshal` and `Unmarshal` you should use the
  minimal type description comming from the Telegram Bot API.
- Do not use lower chars in the types. That seems to cause strange errors.
- Line `answer.Results = []Result{aResult,bResult}`: feel free to add a 2nd, 3rd ...
  result to that array, but change the "0" string (an id) to "1" and "2".
- the html-form file generator is a ugly code part. You may change or
  delete this! The `setWebhook` with cert upload is only need *1 TIMES*

## run it as non root

After `go build` you get an executable/binary like `KonstantOptimalerBot`, but
if you run it `./KonstantOptimalerBot 8443` it should fail.
It is normaly not possible to bind a program to a port below 10000(?) or
sometimes below 40000 as non root user. With the `setcap` command (running as root)
you can make the binary bindable to lower ports for non root users:

```
setcap 'cap_net_bind_service=+ep' ./KonstantOptimalerBot
```

I think you have to do this as root every time you do `go build`.

## let it run after logoff

Take a look/google to the command `nohub` to understand this cli:

```
nohup ./KonstantOptimalerBot 8443 >>/dev/null 2>>/dev/null &
```

## kill it

At that place it is good to known and google the commands `kill` and `ps aux`. With
the last command you see all processes and you are able to find `nohup`
or just the `./KonstantOptimalerBot 8443` process.

## learn and feel free to modify

- code and functions of the bot
- google how to add non-inline commands to the bot
- add handler for `/html/` requests?
- make a linux systemd service from it

## but I want lets-encrypt

If you want to use `certbot` as root user for `let's Encrypt`, this is a good handmade
workaround (90 days valid):

```
 certbot certonly --manual --preferred-challenges dns \
  --register-unsafely-without-email -d bot.yourdomain.com
```

With that command (replace your domain!) a cli dialog starts and you have
to place a special DNS TXT record to verify you domain. After placing that
record... wait 3(?) minutes and enter `Yes`. On success the `fullchain.pem` (cert)
and `privkey.pem` placed to `/etc/certbot/live/bot.yourdomain.com/` or `archive/` ... it is
important to get the real (not symbolic links) files and copy them to
the place, where you execute you golang bot server. Both files must be readable
from the user, who executes the server!

